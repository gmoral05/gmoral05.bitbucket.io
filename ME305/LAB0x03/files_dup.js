var files_dup =
[
    [ "encoder.py", "encoder_8py.html", [
      [ "EncoderDriver", "classencoder_1_1EncoderDriver.html", "classencoder_1_1EncoderDriver" ]
    ] ],
    [ "FSM_encoder.py", "FSM__encoder_8py.html", [
      [ "TaskEncoder", "classFSM__encoder_1_1TaskEncoder.html", "classFSM__encoder_1_1TaskEncoder" ]
    ] ],
    [ "FSM_HW0x00_.py", "FSM__HW0x00___8py.html", [
      [ "TaskElevator1", "classFSM__HW0x00___1_1TaskElevator1.html", "classFSM__HW0x00___1_1TaskElevator1" ],
      [ "TaskElevator2", "classFSM__HW0x00___1_1TaskElevator2.html", "classFSM__HW0x00___1_1TaskElevator2" ],
      [ "Button", "classFSM__HW0x00___1_1Button.html", "classFSM__HW0x00___1_1Button" ],
      [ "MotorDriver", "classFSM__HW0x00___1_1MotorDriver.html", "classFSM__HW0x00___1_1MotorDriver" ]
    ] ],
    [ "FSM_UI.py", "FSM__UI_8py.html", [
      [ "TaskUI", "classFSM__UI_1_1TaskUI.html", "classFSM__UI_1_1TaskUI" ]
    ] ],
    [ "LAB0x02.py", "LAB0x02_8py.html", [
      [ "BlinkVirtual", "classLAB0x02_1_1BlinkVirtual.html", "classLAB0x02_1_1BlinkVirtual" ],
      [ "Blink_Real", "classLAB0x02_1_1Blink__Real.html", "classLAB0x02_1_1Blink__Real" ]
    ] ],
    [ "main_LAB0x03.py", "main__LAB0x03_8py.html", "main__LAB0x03_8py" ],
    [ "me305_Lab1.py", "me305__Lab1_8py.html", "me305__Lab1_8py" ],
    [ "shares.py", "shares_8py.html", "shares_8py" ]
];