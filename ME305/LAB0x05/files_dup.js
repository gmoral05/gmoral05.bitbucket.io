var files_dup =
[
    [ "bluetooth.py", "bluetooth_8py.html", [
      [ "BluetoothDriver", "classbluetooth_1_1BluetoothDriver.html", "classbluetooth_1_1BluetoothDriver" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", [
      [ "EncoderDriver", "classencoder_1_1EncoderDriver.html", "classencoder_1_1EncoderDriver" ]
    ] ],
    [ "FSM_bluetooth.py", "FSM__bluetooth_8py.html", "FSM__bluetooth_8py" ],
    [ "FSM_encoder.py", "FSM__encoder_8py.html", "FSM__encoder_8py" ],
    [ "FSM_HW0x00_.py", "FSM__HW0x00___8py.html", [
      [ "TaskElevator1", "classFSM__HW0x00___1_1TaskElevator1.html", "classFSM__HW0x00___1_1TaskElevator1" ],
      [ "TaskElevator2", "classFSM__HW0x00___1_1TaskElevator2.html", "classFSM__HW0x00___1_1TaskElevator2" ],
      [ "Button", "classFSM__HW0x00___1_1Button.html", "classFSM__HW0x00___1_1Button" ],
      [ "MotorDriver", "classFSM__HW0x00___1_1MotorDriver.html", "classFSM__HW0x00___1_1MotorDriver" ]
    ] ],
    [ "FSM_UI.py", "FSM__UI_8py.html", "FSM__UI_8py" ],
    [ "FSM_UI2.py", "FSM__UI2_8py.html", "FSM__UI2_8py" ],
    [ "LAB0x02.py", "LAB0x02_8py.html", [
      [ "BlinkVirtual", "classLAB0x02_1_1BlinkVirtual.html", "classLAB0x02_1_1BlinkVirtual" ],
      [ "Blink_Real", "classLAB0x02_1_1Blink__Real.html", "classLAB0x02_1_1Blink__Real" ]
    ] ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main_LAB0x03.py", "main__LAB0x03_8py.html", "main__LAB0x03_8py" ],
    [ "me305_Lab1.py", "me305__Lab1_8py.html", "me305__Lab1_8py" ],
    [ "shares.py", "shares_8py.html", "shares_8py" ]
];