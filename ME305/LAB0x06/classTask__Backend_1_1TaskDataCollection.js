var classTask__Backend_1_1TaskDataCollection =
[
    [ "__init__", "classTask__Backend_1_1TaskDataCollection.html#ac7fe04dcdebac8820d797bf45c857427", null ],
    [ "run", "classTask__Backend_1_1TaskDataCollection.html#af136630d13dffadff9272426f3697e9b", null ],
    [ "transitionTo", "classTask__Backend_1_1TaskDataCollection.html#a7fe6b632d61b34cfe7acd5e00e02cfc3", null ],
    [ "curr_time", "classTask__Backend_1_1TaskDataCollection.html#a01fb55824b7ab097893ddf0434375577", null ],
    [ "EncoderDriver", "classTask__Backend_1_1TaskDataCollection.html#a008bfe8448515957a7a3a4e8fcaf281b", null ],
    [ "interval", "classTask__Backend_1_1TaskDataCollection.html#a7bad76857a000b1442911be434613944", null ],
    [ "Kp", "classTask__Backend_1_1TaskDataCollection.html#a140f152be75f9f20994dc6157a185dee", null ],
    [ "MotorDriver", "classTask__Backend_1_1TaskDataCollection.html#aa2c42c1e88cc62f6d883ca16db79a0b9", null ],
    [ "next_time", "classTask__Backend_1_1TaskDataCollection.html#aae352f3a6743a0bb37d0eda5bd341eb2", null ],
    [ "omega", "classTask__Backend_1_1TaskDataCollection.html#acd4959f6777fb8ca54179adf851a7278", null ],
    [ "omega_array", "classTask__Backend_1_1TaskDataCollection.html#a6483b5238a78c7f6d3becd88b9973519", null ],
    [ "omega_ref", "classTask__Backend_1_1TaskDataCollection.html#ad6939ff14c5b21269eb6db0a533eb3be", null ],
    [ "omega_ref_array", "classTask__Backend_1_1TaskDataCollection.html#a3c628431222bc2f24c5f5d15fa3a981b", null ],
    [ "pos_length", "classTask__Backend_1_1TaskDataCollection.html#a92090c7030ba88bc49d46465a833386e", null ],
    [ "runs", "classTask__Backend_1_1TaskDataCollection.html#aa0551f548411b08858588da8e86a44f2", null ],
    [ "ser", "classTask__Backend_1_1TaskDataCollection.html#a42819e8059ef010142622e1a2ee7f379", null ],
    [ "start_new_time", "classTask__Backend_1_1TaskDataCollection.html#a872fe3d8c74fcecf1f7aad00a1235969", null ],
    [ "start_time", "classTask__Backend_1_1TaskDataCollection.html#afb0a9777c1624b467ca5156a98af359b", null ],
    [ "state", "classTask__Backend_1_1TaskDataCollection.html#a53cd0db21a1cab0c1a9633e511375933", null ],
    [ "time_array", "classTask__Backend_1_1TaskDataCollection.html#afef127280a74dc77b53abd52baa0eddb", null ]
];