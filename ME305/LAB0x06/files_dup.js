var files_dup =
[
    [ "bluetooth.py", "bluetooth_8py.html", [
      [ "BluetoothDriver", "classbluetooth_1_1BluetoothDriver.html", "classbluetooth_1_1BluetoothDriver" ]
    ] ],
    [ "ClosedLoopController.py", "ClosedLoopController_8py.html", [
      [ "ClosedLoop", "classClosedLoopController_1_1ClosedLoop.html", "classClosedLoopController_1_1ClosedLoop" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", [
      [ "EncoderDriver", "classencoder_1_1EncoderDriver.html", "classencoder_1_1EncoderDriver" ]
    ] ],
    [ "FSM_bluetooth.py", "FSM__bluetooth_8py.html", "FSM__bluetooth_8py" ],
    [ "FSM_encoder.py", "FSM__encoder_8py.html", "FSM__encoder_8py" ],
    [ "FSM_HW0x00_.py", "FSM__HW0x00___8py.html", [
      [ "TaskElevator1", "classFSM__HW0x00___1_1TaskElevator1.html", "classFSM__HW0x00___1_1TaskElevator1" ],
      [ "TaskElevator2", "classFSM__HW0x00___1_1TaskElevator2.html", "classFSM__HW0x00___1_1TaskElevator2" ],
      [ "Button", "classFSM__HW0x00___1_1Button.html", "classFSM__HW0x00___1_1Button" ],
      [ "MotorDriver", "classFSM__HW0x00___1_1MotorDriver.html", "classFSM__HW0x00___1_1MotorDriver" ]
    ] ],
    [ "FSM_UI.py", "FSM__UI_8py.html", "FSM__UI_8py" ],
    [ "FSM_UI2.py", "FSM__UI2_8py.html", "FSM__UI2_8py" ],
    [ "LAB0x02.py", "LAB0x02_8py.html", [
      [ "BlinkVirtual", "classLAB0x02_1_1BlinkVirtual.html", "classLAB0x02_1_1BlinkVirtual" ],
      [ "Blink_Real", "classLAB0x02_1_1Blink__Real.html", "classLAB0x02_1_1Blink__Real" ]
    ] ],
    [ "main_HW0x00.py", "main__HW0x00_8py.html", "main__HW0x00_8py" ],
    [ "main_LAB0x02.py", "main__LAB0x02_8py.html", "main__LAB0x02_8py" ],
    [ "main_LAB0x03.py", "main__LAB0x03_8py.html", "main__LAB0x03_8py" ],
    [ "me305_Lab1.py", "me305__Lab1_8py.html", "me305__Lab1_8py" ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "Task_Backend.py", "Task__Backend_8py.html", "Task__Backend_8py" ],
    [ "Task_Control.py", "Task__Control_8py.html", [
      [ "Task_Control", "classTask__Control_1_1Task__Control.html", "classTask__Control_1_1Task__Control" ]
    ] ],
    [ "Task_FrontEnd.py", "Task__FrontEnd_8py.html", "Task__FrontEnd_8py" ]
];