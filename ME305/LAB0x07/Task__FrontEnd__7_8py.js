var Task__FrontEnd__7_8py =
[
    [ "return_deg", "Task__FrontEnd__7_8py.html#a7de697382330d3a4a7616ec7d74afae4", null ],
    [ "return_deg_ref", "Task__FrontEnd__7_8py.html#a2443467e0a2f26e324ba19d49ab85950", null ],
    [ "return_omega", "Task__FrontEnd__7_8py.html#aa85d1182461bea3b436a6ca6cada70bb", null ],
    [ "return_omega_ref", "Task__FrontEnd__7_8py.html#ad1a3e6061494b1cd9e352286d0e494f9", null ],
    [ "return_tim", "Task__FrontEnd__7_8py.html#a156c6594dac5b6fb41f15b8912e0ce95", null ],
    [ "curr_time", "Task__FrontEnd__7_8py.html#a9271e5033116dceba133b7d4fd5812fe", null ],
    [ "deg_col", "Task__FrontEnd__7_8py.html#af017c4710d75f3cb15b032c1122d6f9b", null ],
    [ "degref_col", "Task__FrontEnd__7_8py.html#a346688fd762eeb7094dd099c8c223265", null ],
    [ "Kp", "Task__FrontEnd__7_8py.html#aa12ae860438920f3329d77e64caea921", null ],
    [ "plots", "Task__FrontEnd__7_8py.html#a697edd10bb9c2c299b9d4120afdf0eeb", null ],
    [ "ser", "Task__FrontEnd__7_8py.html#a56340e24ce4fd0466ae812c008b5d02a", null ],
    [ "speed_col", "Task__FrontEnd__7_8py.html#ac6290c262362bbdb0fc088ce388c8722", null ],
    [ "speedref_col", "Task__FrontEnd__7_8py.html#a05287e935365e8156161849a4c7ac67e", null ],
    [ "start_time", "Task__FrontEnd__7_8py.html#a03387783c314c1c5bbaa40eb2643680a", null ],
    [ "tim_col", "Task__FrontEnd__7_8py.html#a95c86102334bdd8073981ad6773e64db", null ]
];