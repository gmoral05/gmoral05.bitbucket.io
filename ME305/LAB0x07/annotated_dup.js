var annotated_dup =
[
    [ "bluetooth", null, [
      [ "BluetoothDriver", "classbluetooth_1_1BluetoothDriver.html", "classbluetooth_1_1BluetoothDriver" ]
    ] ],
    [ "ClosedLoopController", null, [
      [ "ClosedLoop", "classClosedLoopController_1_1ClosedLoop.html", "classClosedLoopController_1_1ClosedLoop" ]
    ] ],
    [ "encoder", null, [
      [ "EncoderDriver", "classencoder_1_1EncoderDriver.html", "classencoder_1_1EncoderDriver" ]
    ] ],
    [ "encoder_7", null, [
      [ "EncoderDriver", "classencoder__7_1_1EncoderDriver.html", "classencoder__7_1_1EncoderDriver" ]
    ] ],
    [ "FSM_bluetooth", null, [
      [ "Task_BLE", "classFSM__bluetooth_1_1Task__BLE.html", "classFSM__bluetooth_1_1Task__BLE" ]
    ] ],
    [ "FSM_encoder", null, [
      [ "TaskEncoder", "classFSM__encoder_1_1TaskEncoder.html", "classFSM__encoder_1_1TaskEncoder" ]
    ] ],
    [ "FSM_HW0x00_", null, [
      [ "Button", "classFSM__HW0x00___1_1Button.html", "classFSM__HW0x00___1_1Button" ],
      [ "MotorDriver", "classFSM__HW0x00___1_1MotorDriver.html", "classFSM__HW0x00___1_1MotorDriver" ],
      [ "TaskElevator1", "classFSM__HW0x00___1_1TaskElevator1.html", "classFSM__HW0x00___1_1TaskElevator1" ],
      [ "TaskElevator2", "classFSM__HW0x00___1_1TaskElevator2.html", "classFSM__HW0x00___1_1TaskElevator2" ]
    ] ],
    [ "FSM_UI", null, [
      [ "TaskUI", "classFSM__UI_1_1TaskUI.html", "classFSM__UI_1_1TaskUI" ]
    ] ],
    [ "LAB0x02", null, [
      [ "Blink_Real", "classLAB0x02_1_1Blink__Real.html", "classLAB0x02_1_1Blink__Real" ],
      [ "BlinkVirtual", "classLAB0x02_1_1BlinkVirtual.html", "classLAB0x02_1_1BlinkVirtual" ]
    ] ],
    [ "main_LAB0x04", null, [
      [ "TaskDataCollection", "classmain__LAB0x04_1_1TaskDataCollection.html", "classmain__LAB0x04_1_1TaskDataCollection" ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "MotorDriver_7", null, [
      [ "MotorDriver", "classMotorDriver__7_1_1MotorDriver.html", "classMotorDriver__7_1_1MotorDriver" ]
    ] ],
    [ "PIController_7", null, [
      [ "ClosedLoop", "classPIController__7_1_1ClosedLoop.html", "classPIController__7_1_1ClosedLoop" ]
    ] ],
    [ "Task_Backend", null, [
      [ "TaskDataCollection", "classTask__Backend_1_1TaskDataCollection.html", "classTask__Backend_1_1TaskDataCollection" ]
    ] ],
    [ "Task_Backend_7", null, [
      [ "TaskDataCollection", "classTask__Backend__7_1_1TaskDataCollection.html", "classTask__Backend__7_1_1TaskDataCollection" ]
    ] ],
    [ "Task_Control", null, [
      [ "Task_Control", "classTask__Control_1_1Task__Control.html", "classTask__Control_1_1Task__Control" ]
    ] ],
    [ "Task_Control_7", null, [
      [ "Task_Control", "classTask__Control__7_1_1Task__Control.html", "classTask__Control__7_1_1Task__Control" ]
    ] ]
];