var classPIController__7_1_1ClosedLoop =
[
    [ "__init__", "classPIController__7_1_1ClosedLoop.html#a37e48e034aa936e4964cfd79d3a3a97e", null ],
    [ "get_Kp", "classPIController__7_1_1ClosedLoop.html#af36242c711a31f4b716ac65bb33d082f", null ],
    [ "set_Kp", "classPIController__7_1_1ClosedLoop.html#a555cfb3de2af3cf2bd071cf41196f670", null ],
    [ "update", "classPIController__7_1_1ClosedLoop.html#a448d2b0fabb52fc28ca017ec5f6084de", null ],
    [ "EncoderDriver", "classPIController__7_1_1ClosedLoop.html#adb905728ce45d48edb928ec971d2e59f", null ],
    [ "Kp", "classPIController__7_1_1ClosedLoop.html#a6580a30beda2e49b5badd7d383de20e9", null ],
    [ "MotorDriver", "classPIController__7_1_1ClosedLoop.html#a4db37770b433a38a3b34b811aafee660", null ]
];