var searchData=
[
  ['omega_78',['omega',['../shares_8py.html#af894646e4711cb6ed98c6af5298b0126',1,'shares.omega()'],['../shares__7_8py.html#ade36c58ed91f43992470d9173f3d160a',1,'shares_7.omega()'],['../shares__LAB6_8py.html#a91f4f1746671fab3f7e386ba86b307f0',1,'shares_LAB6.omega()']]],
  ['omega_5farray_79',['omega_array',['../classTask__Backend_1_1TaskDataCollection.html#a6483b5238a78c7f6d3becd88b9973519',1,'Task_Backend.TaskDataCollection.omega_array()'],['../classTask__Backend__7_1_1TaskDataCollection.html#a9e86630b0d055a709286a9d4d192c85c',1,'Task_Backend_7.TaskDataCollection.omega_array()']]],
  ['omega_5fref_80',['omega_ref',['../shares_8py.html#acfaf6fb13afb8b190068d18e650514b8',1,'shares.omega_ref()'],['../shares__7_8py.html#ad21366ddff8328c18746d55d5d676acf',1,'shares_7.omega_ref()'],['../shares__LAB6_8py.html#a952ebeecfc650bb8b847afa84efcefa8',1,'shares_LAB6.omega_ref()']]]
];
