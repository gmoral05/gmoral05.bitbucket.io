var Task__FrontEnd_8py =
[
    [ "return_omega", "Task__FrontEnd_8py.html#af493ed5d89a4d3effac6d47be1a6ac58", null ],
    [ "return_omega_ref", "Task__FrontEnd_8py.html#a798583ca949afcdf848e89839ca321b0", null ],
    [ "return_tim", "Task__FrontEnd_8py.html#a56628cb52ee8434beed278d25bed0461", null ],
    [ "curr_time", "Task__FrontEnd_8py.html#a1b8b6480bab50b93dbe10652a7956f6e", null ],
    [ "Kp", "Task__FrontEnd_8py.html#a505e30ab6058709977748244d22372cc", null ],
    [ "omega_ref", "Task__FrontEnd_8py.html#a09a9fb0dc8e270a9bb6b05a8b51fb45b", null ],
    [ "plots", "Task__FrontEnd_8py.html#aa92e3edd3dd8ec89ffb267a0c71f7eaa", null ],
    [ "ser", "Task__FrontEnd_8py.html#ad2237f09b73751f813bcc326e347f5a3", null ],
    [ "start_time", "Task__FrontEnd_8py.html#a347fa1706510b3df25bdfcea8aaafbe9", null ],
    [ "x_col", "Task__FrontEnd_8py.html#ae8c12cc4efee6db096c1e14af9dfe84c", null ],
    [ "y_col", "Task__FrontEnd_8py.html#a2e03cd1f19322452cd8c5c16e711c2b4", null ],
    [ "yy_col", "Task__FrontEnd_8py.html#abe11497b676241f51fd66ebf4b367b18", null ]
];