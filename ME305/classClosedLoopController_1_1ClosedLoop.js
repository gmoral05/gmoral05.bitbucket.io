var classClosedLoopController_1_1ClosedLoop =
[
    [ "__init__", "classClosedLoopController_1_1ClosedLoop.html#a470d3292de17d73f0b34e6aba61c5938", null ],
    [ "get_Kp", "classClosedLoopController_1_1ClosedLoop.html#a4bd016670ca2728742633f6bbfa2ac82", null ],
    [ "set_Kp", "classClosedLoopController_1_1ClosedLoop.html#a7494c2e16f65815014929303dc374267", null ],
    [ "update", "classClosedLoopController_1_1ClosedLoop.html#a87c0e00e4d4715672e859c2461e835fe", null ],
    [ "EncoderDriver", "classClosedLoopController_1_1ClosedLoop.html#afcb971175be576a6a495e07d902ecf91", null ],
    [ "Kp", "classClosedLoopController_1_1ClosedLoop.html#a3ce4ce15bffb07d7b3b39d9718a056ff", null ],
    [ "MotorDriver", "classClosedLoopController_1_1ClosedLoop.html#ac769b1f945457ffaaecbe68d78f393df", null ]
];