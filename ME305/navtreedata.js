/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 305 Documentation Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "LAB0x01: Fibonacci", "index.html#page_1", null ],
    [ "LAB0x02: FSM_LED_TriangleWave", "index.html#sec_page_1", null ],
    [ "LAB0x03: Incremental Encoders", "index.html#sec_lab3", null ],
    [ "LAB0x04: Interface Extension", "index.html#sec_lab4", null ],
    [ "LAB0x05: Bluetooth LED Control", "index.html#sec_lab5", null ],
    [ "LAB0x06: MotorDriver", "index.html#sec_lab6", null ],
    [ "LAB0x07: Reference Tracking", "index.html#sec_lab7", null ],
    [ "HW0x00: FSM_Elevator", "index.html#sec_HW0x00", null ],
    [ "LAB0x05: Source Code & Diagrams", "page_LAB5.html", [
      [ "Source Code Access & Android APP", "page_LAB5.html#page_lab5_src", null ]
    ] ],
    [ "LAB0x06: Source Code & Diagrams", "page_LAB6.html", [
      [ "Source Code Access", "page_LAB6.html#page_lab6_src", null ]
    ] ],
    [ "LAB0x07: Source Code & Diagrams", "page_LAB7.html", [
      [ "Source Code Access", "page_LAB7.html#page_lab7_src", null ]
    ] ],
    [ "LAB0x03: Source Code & Diagrams", "page_LAB3.html", [
      [ "Source Code Access", "page_LAB3.html#page_lab3_src", null ]
    ] ],
    [ "HW0x00: Source Code & Diagrams", "page_HW.html", [
      [ "Source Code Access", "page_HW.html#page_hw_src", null ]
    ] ],
    [ "LAB0x04: Source Code & Diagrams", "page_LAB4.html", [
      [ "Source Code Access", "page_LAB4.html#page_lab4_src", null ]
    ] ],
    [ "LAB0x02: Source Code & Diagrams", "page_LAB2.html", [
      [ "Source Code Access", "page_LAB2.html#page_lab2_src", null ]
    ] ],
    [ "LAB0x01: Source Code & Diagrams", "page_fib.html", [
      [ "Source Code Access", "page_fib.html#ppage_fib_src", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"ClosedLoopController_8py.html",
"classTask__Backend_1_1TaskDataCollection.html#a53cd0db21a1cab0c1a9633e511375933",
""
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';