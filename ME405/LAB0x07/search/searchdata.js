var indexSectionsWithContent =
{
  0: "_abcdefgilmoprstuxyz",
  1: "mt",
  2: "m",
  3: "lmt",
  4: "_bcfgoprstxyz",
  5: "abcdeilmprstuxy",
  6: "l"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

