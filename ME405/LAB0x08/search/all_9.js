var searchData=
[
  ['lab0x01_2epy_47',['LAB0x01.py',['../LAB0x01_8py.html',1,'']]],
  ['lab0x02_2epy_48',['LAB0x02.py',['../LAB0x02_8py.html',1,'']]],
  ['lab0x04_5fplotting_2epy_49',['Lab0x04_Plotting.py',['../Lab0x04__Plotting_8py.html',1,'']]],
  ['lab0x08_5ftest_2epy_50',['Lab0x08_test.py',['../Lab0x08__test_8py.html',1,'']]],
  ['led_51',['led',['../LAB0x02_8py.html#af88fbdd98d52181e63899c8fdb3a59d3',1,'LAB0x02']]],
  ['lx_52',['Lx',['../classtouch_1_1touch.html#ae5612d8172ef8182e9a9b9f96b912898',1,'touch::touch']]],
  ['ly_53',['Ly',['../classtouch_1_1touch.html#a3478df78823af7081f662f5d04073cf6',1,'touch::touch']]],
  ['lab0x05_3a_20equations_54',['LAB0x05: Equations',['../page_LAB5.html',1,'']]],
  ['lab0x06_3a_20simulation_20plots_20_26_20discussion_55',['LAB0x06: Simulation Plots &amp; Discussion',['../page_LAB6.html',1,'']]],
  ['lab0x07_3a_20touch_20panel_20testing_56',['LAB0x07: Touch Panel Testing',['../page_LAB7.html',1,'']]]
];
