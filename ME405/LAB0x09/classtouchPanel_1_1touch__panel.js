var classtouchPanel_1_1touch__panel =
[
    [ "__init__", "classtouchPanel_1_1touch__panel.html#ac763acdfc26ea1c68476d910f01035f9", null ],
    [ "calculate", "classtouchPanel_1_1touch__panel.html#abd710008ce8bff4d63b90ef984516055", null ],
    [ "read", "classtouchPanel_1_1touch__panel.html#a6b031635dc2f5ddb4cd8540bf8612ea6", null ],
    [ "readRaw", "classtouchPanel_1_1touch__panel.html#ac21addf772087257e6c8944cdfd4c944", null ],
    [ "readX", "classtouchPanel_1_1touch__panel.html#a6380c2734dc00d1ccfb7c04574174912", null ],
    [ "readY", "classtouchPanel_1_1touch__panel.html#a2b0ae610fef32c592a46597ff0566a1d", null ],
    [ "readZ", "classtouchPanel_1_1touch__panel.html#a7cc2acf98f83e37d82fa798fc1617c5b", null ],
    [ "center", "classtouchPanel_1_1touch__panel.html#a4dbc287963c8b16fb41e87c0157030a3", null ],
    [ "dims", "classtouchPanel_1_1touch__panel.html#a659f0576bffec5a59ab4a96bb2dc9e3a", null ],
    [ "downBound", "classtouchPanel_1_1touch__panel.html#af44e0ba31865ec8cfe684a08861c3cb2", null ],
    [ "leftBound", "classtouchPanel_1_1touch__panel.html#a2b800bc1236270457ae6fb288f788a96", null ],
    [ "res", "classtouchPanel_1_1touch__panel.html#aa95f7b13d8532cd9dc61a745439325aa", null ],
    [ "rightBound", "classtouchPanel_1_1touch__panel.html#ab9121e486bd23abbe5b0bc80a3bff2e3", null ],
    [ "upBound", "classtouchPanel_1_1touch__panel.html#a1fac4b6c556726b5e82958fbe2661c26", null ],
    [ "xm", "classtouchPanel_1_1touch__panel.html#a7818a0f68e0ee40784acd3d510dee108", null ],
    [ "xp", "classtouchPanel_1_1touch__panel.html#ac1ab42698bbe4ef2bc2ba190b6cc3909", null ],
    [ "ym", "classtouchPanel_1_1touch__panel.html#a9653c8823cbad39ec3a26c9eb54df534", null ],
    [ "yp", "classtouchPanel_1_1touch__panel.html#aff86cd4bc590fdf4df3136f897bf719a", null ]
];