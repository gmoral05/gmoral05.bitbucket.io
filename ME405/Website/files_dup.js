var files_dup =
[
    [ "controller.py", "controller_8py.html", [
      [ "fullState", "classcontroller_1_1fullState.html", "classcontroller_1_1fullState" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", "encoder_8py" ],
    [ "LAB0x01.py", "LAB0x01_8py.html", "LAB0x01_8py" ],
    [ "LAB0x02.py", "LAB0x02_8py.html", "LAB0x02_8py" ],
    [ "Lab0x04_Plotting.py", "Lab0x04__Plotting_8py.html", "Lab0x04__Plotting_8py" ],
    [ "Lab0x08_test.py", "Lab0x08__test_8py.html", "Lab0x08__test_8py" ],
    [ "Lab0x09_main.py", "Lab0x09__main_8py.html", "Lab0x09__main_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "mcp9808.py", "mcp9808_8py.html", [
      [ "MCP9808", "classmcp9808_1_1MCP9808.html", "classmcp9808_1_1MCP9808" ]
    ] ],
    [ "MotorDriver.py", "MotorDriver_8py.html", "MotorDriver_8py" ],
    [ "touch.py", "touch_8py.html", "touch_8py" ],
    [ "touchPanel.py", "touchPanel_8py.html", "touchPanel_8py" ]
];