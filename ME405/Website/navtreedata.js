/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405 Documentation Portfolio", "index.html", [
    [ "Portfolio Table of Contents", "index.html#sec_port", null ],
    [ "Lab0x01 Documentation", "index.html#sec_lab1", null ],
    [ "Lab0x02 Documentation", "index.html#sec_lab2", null ],
    [ "Lab0x03 Documentation", "index.html#sec_lab3", null ],
    [ "Lab0x04 Documentation", "index.html#sec_lab4", null ],
    [ "Lab0x05 Documentation", "index.html#sec_lab5", null ],
    [ "Lab0x06 Documentation", "index.html#sec_lab6", null ],
    [ "Lab0x07 Documentation", "index.html#sec_lab7", null ],
    [ "Lab0x08 Documentation", "index.html#sec_lab8", null ],
    [ "Lab0x09 Documentation", "index.html#sec_lab9", null ],
    [ "LAB0x05: Equations", "page_LAB5.html", [
      [ "Equations", "page_LAB5.html#page_lab5_src", null ]
    ] ],
    [ "LAB0x06: Simulation Plots & Discussion", "page_LAB6.html", [
      [ "Part 3A: (x, theta, x_dot, theta_dot)= (0, 0,0,0)", "page_LAB6.html#sec_3A", null ],
      [ "Part 3B: (x, theta, x_dot, theta_dot)= (0.5, 0,0,0)", "page_LAB6.html#sec_3B", null ],
      [ "Part 3C: (x, theta, x_dot, theta_dot)= (0, 5,0,0)", "page_LAB6.html#sec_3C", null ],
      [ "Part 3D: Impulse", "page_LAB6.html#sec_3D", null ],
      [ "Part 4: [K]= (0.3,0.2,0.05,0.02)", "page_LAB6.html#sec_4", null ]
    ] ],
    [ "LAB0x07: Touch Panel Testing", "page_LAB7.html", [
      [ "Set-up", "page_LAB7.html#Set-up", null ],
      [ "Time", "page_LAB7.html#Testing", null ],
      [ "Readings", "page_LAB7.html#Readings", null ]
    ] ],
    [ "Term Project: Testing & Result", "page_LAB9.html", [
      [ "Hardware Set-up", "page_LAB9.html#sec_9A", null ],
      [ "Analytical Calculations", "page_LAB9.html#sec_9B", null ],
      [ "Testing Results", "page_LAB9.html#sec_9C", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"index.html#sec_lab4"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';