var indexSectionsWithContent =
{
  0: "_abcdefgiklmnoprstuvwxyz",
  1: "defmt",
  2: "mt",
  3: "celmt",
  4: "_bcdefgoprstuwxyz",
  5: "abcdefiklmnoprstuvxy",
  6: "lt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

